// Objects
	// used to represent real world objects
	// information in object are stored in "key: value" pair

// Create object using object initializers/ literal notation

/*
Syntax:
	let objectName = {
		keyA: value,
		keyB: valueB
	}
*/
console.log(">>>--- Object ---<<<");
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};
console.log('Object created through intializer:');
console.log(cellphone);
console.log(typeof cellphone);
console.log("");
console.log("");

// Create object using constructor function

/*
	Syntax:
		function objectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		}
new ---> creates a new object out of the function without reassigning the original/initial object function which is why it is ideal for repetitions. 
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
};

let laptop = new Laptop('Lenovo', 2008);
console.log('Object through constructor function:')
console.log(laptop);

let myLaptop = new Laptop('Macbook Air', 2020);
console.log('Object through constructor function again:');
console.log(myLaptop);
console.log("");
console.log("");


console.log(">>>--- Sample of Undefined ---<<<");
let oldLaptop = Laptop('Portal R2E CCMC', 1980);
console.log('Object through contructor function without new keyboard:');
console.log(oldLaptop);
// undefined
console.log("");
console.log("");


console.log(">>>--- Sample of No Result ---<<<");
Laptop('Portal R2E CCMC', 1980);
// no result from plain invocation
console.log("");
console.log("");

// Creating empty objects
console.log(">>>--- Creating empty objects ---<<<");
let computer = {};
let myComputer = new Object();
console.log("");
console.log("");


// Accessing object properties

// using dot notation
// Syntax: objectName.property
console.log(">>>--- using dot notation ---<<<");
console.log('Result from dot notation: ' + myLaptop.name);
console.log("");
console.log("");


// using square bracket notation
// Syntax: objectName['property']
console.log(">>>--- using square bracket notation ---<<<");
console.log('Result from bracket notation: ' + myLaptop['name']);

console.log(`Result from dot notation ${laptop.name}`);
console.log(`Result from dot notation with non-existing property ${laptop.specs}`);
console.log("");
console.log("");

// result: undefined

// accessing array objects
console.log(">>>--- accessing array objects ---<<<");
let array = [laptop, myLaptop];
console.log("");
console.log("");


// bracket notation
console.log(">>>--- accessing array objects through bracket notation ---<<<");
console.log(`Result from bracket notation: ${array[0]['name']}`);
console.log("");
console.log("");

// dot notation
console.log(">>>--- accessing array objects through dot notation ---<<<");
console.log(`Result from dot notation: ${array[1].manufactureDate}`);
console.log("");
console.log("");


/* Mini-Activity
	
};
*/

let person = {
	firstName: 'Levi',
	lastName: 'Ackerman',
	password: 'notInAWheelChair',
	email: 'levi@gmail.com',
	age: 30
};

console.log(`first name: ${person.firstName}`);
console.log(`last name: ${person.lastName}`);
console.log(`password: ${person.password}`);
console.log(`email: ${person.email}`);
console.log(`age: ${person.age}`);
console.log("");
console.log("");



// Initialize or add objeect properties using dot notation
console.log(">>>--- Initialize or add objeect properties using dot notation ---<<<");

let car = {};
car.name = 'Honda Civic';
console.log('Result from adding property through dot notation: ' + car);
console.log(car);

console.log("");
console.log("");

// using bracket notation
console.log(">>>--- using bracket notation ---<<<");
car['manufacture date'] = 2019
console.log(`Result from adding property through bracket notation:`);
console.log(car);
console.log("");
console.log("");

// deleting object properties
console.log(">>>--- deleting object properties ---<<<");
delete car['manufacture date'];
console.log('Result from deleting');
console.log(car);
console.log("");
console.log("");

// re-ssigning object properties
console.log(">>>--- re-ssigning object properties ---<<<")
car.name = 'Dodge Charger R/T';
console.log('Result from reassignment:');
console.log(car);
console.log("");
console.log("");

// Object Methods
console.log(">>>--- Object Methods ---<<<");
let person1 = {
	name: 'John',
	talk: function(){
		console.log(`Hello my name is ${this.name}`);
	}
};
console.log(person1);
console.log('Result from object method:');
person1.talk();
console.log("");
console.log("");

// adding methods to our objects
console.log(">>>--- adding methods to our objects ---<<<");
person1.walk = function(){
	console.log(`${this.name} walked 25 steps forward`);
}
person1.walk();
console.log("");
console.log("");

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@emai.xyz'],
	introduce: function(){
		console.log(`Hello my name is ${this.firstName} ${this.lastName}. live at ${this.address.city} and my personal email is ${this.emails[0]}`)
	}
};
friend.introduce();
console.log("");
console.log("");

// real world application of objects
console.log(">>>--- real world application of objects ---<<<");

console.log(">>>--- Object constructor ---<<<");
function Pokemon(name, level){

	// Properties
	this.name = name;
	this.lvl = level;
	this.health = 2 * level;
	this.atk = level;

	// Methods
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`targetPokemon's health now reduced to _targetPokemonHealth_`);
	};

	this.faint = function(){
		console.log(`${this.name} fainted`)
	};
};

let bulbasaur = new Pokemon("bulbasaur", 16);
let rattata = new Pokemon("rattata", 8);
console.log(bulbasaur);
console.log(rattata);

bulbasaur.tackle(rattata);
rattata.tackle(bulbasaur);
bulbasaur.faint();
